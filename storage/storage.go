package storage

import (
	"context"

	"gitlab.com/car_rental/car_service/genproto/car_service"
)

type StorageI interface {
	CloseDB()
	Car() CarRepoI
	Brand() BrandRepoI
	Model() ModelRepoI
	CarActivity() CarActivityRepoI
}

type CarRepoI interface {
	Create(context.Context, *car_service.CarCreate) (*car_service.Car, error)
	GetByID(context.Context, *car_service.CarPrimaryKey) (*car_service.Car, error)
	GetList(context.Context, *car_service.CarGetListRequest) (*car_service.CarGetListResponse, error)
	Update(context.Context, *car_service.CarUpdate) (*car_service.Car, error)
	Delete(context.Context, *car_service.CarPrimaryKey) error
}

type BrandRepoI interface {
	Create(context.Context, *car_service.BrandCreate) (*car_service.Brand, error)
	GetByID(context.Context, *car_service.BrandPrimaryKey) (*car_service.Brand, error)
	GetList(context.Context, *car_service.BrandGetListRequest) (*car_service.BrandGetListResponse, error)
	Update(context.Context, *car_service.BrandUpdate) (*car_service.Brand, error)
	Delete(context.Context, *car_service.BrandPrimaryKey) error
}

type ModelRepoI interface {
	Create(context.Context, *car_service.ModelCreate) (*car_service.Model, error)
	GetByID(context.Context, *car_service.ModelPrimaryKey) (*car_service.Model, error)
	GetList(context.Context, *car_service.ModelGetListRequest) (*car_service.ModelGetListResponse, error)
	Update(context.Context, *car_service.ModelUpdate) (*car_service.Model, error)
	Delete(context.Context, *car_service.ModelPrimaryKey) error
}

type CarActivityRepoI interface {
	Create(context.Context, *car_service.CarActivityCreate) (*car_service.CarActivity, error)
	GetList(context.Context, *car_service.CarActivityGetListRequest) (*car_service.CarActivityResp, error)
}
