package postgres

import (
	"context"
	"database/sql"
	"fmt"

	"github.com/jackc/pgx/v4/pgxpool"

	"gitlab.com/car_rental/car_service/genproto/car_service"
)

type CarActivityRepo struct {
	db *pgxpool.Pool
}

func NewCarActivityRepo(db *pgxpool.Pool) *CarActivityRepo {
	return &CarActivityRepo{
		db: db,
	}
}

func (r *CarActivityRepo) Create(ctx context.Context, req *car_service.CarActivityCreate) (*car_service.CarActivity, error) {

	var (
		query string
	)

	query = `
		INSERT INTO car_report(state_number, car_id, status , "date")
		VALUES ($1, $2, $3 , NOW())
	`

	_, err := r.db.Exec(ctx, query,
		req.StateNumber,
		req.CarId,
		req.Status,
	)

	if err != nil {
		return nil, err
	}

	return &car_service.CarActivity{
		StateNumber: req.StateNumber,
		CarId:       req.CarId,
		Status:      req.Status,
		Date:        req.Date,
	}, nil
}

func (r *CarActivityRepo) GetList(ctx context.Context, req *car_service.CarActivityGetListRequest) (*car_service.CarActivityResp, error) {

	var (
		resp    = &car_service.CarActivityResp{}
		query   string
		where   = " WHERE TRUE "
		offset  = " OFFSET 0"
		limit   = " LIMIT 10"
		from    = "FROM " + req.From
		to      = "TO" + req.To
		groupBy = " GROUP BY state_number ,car_id, status , date "
		orderBy = " ORDER BY date(date)"
	)

	query = `
		SELECT
			state_number,
			car_id,
			status,
			date
		FROM car_report
		
	`

	if req.Offset > 0 {
		offset = fmt.Sprintf(" OFFSET %d", req.Offset)
	}

	if req.Limit > 0 {
		limit = fmt.Sprintf(" LIMIT %d", req.Limit)
	}

	if req.From > from {
		from = fmt.Sprintf(" FROM %s", req.From)
	}
	if req.To > to {
		to = fmt.Sprintf(" TO %s", req.To)
	}
	// if req.Search != "" {
	// 	where += ` AND state_number ILIKE '%' || '` + req.Search + `' || '%'`
	// }
	if len(req.From) == 0 || len(req.To) == 0 {
		query += where + groupBy + orderBy + offset + limit
	} else {
		query += where + from + to + groupBy + orderBy + offset + limit
	}
	fmt.Println(query)
	rows, err := r.db.Query(ctx, query)
	if err != nil {
		return nil, err
	}

	for rows.Next() {
		var (
			state_number sql.NullString
			car_id       sql.NullString
			status       sql.NullString
			date         sql.NullString
		)

		err := rows.Scan(
			&state_number,
			&car_id,
			&status,
			&date,
		)

		if err != nil {
			return nil, err
		}

		resp.Res = append(resp.Res, &car_service.CarActivity{
			StateNumber: state_number.String,
			CarId:       car_id.String,
			Status:      status.String,
			Date:        date.String,
		})
	}
	rows.Close()

	return resp, nil
}
