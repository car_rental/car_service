package postgres

import (
	"context"
	"database/sql"
	"fmt"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"

	"gitlab.com/car_rental/car_service/genproto/car_service"
	"gitlab.com/car_rental/car_service/pkg/helper"
)

type BrandRepo struct {
	db *pgxpool.Pool
}

func NewBrandRepo(db *pgxpool.Pool) *BrandRepo {
	return &BrandRepo{
		db: db,
	}
}

func (r *BrandRepo) Create(ctx context.Context, req *car_service.BrandCreate) (*car_service.Brand, error) {

	var (
		id    = uuid.New().String()
		query string
	)

	query = `
		INSERT INTO brand(id, photo ,name,updated_at)
		VALUES ($1, $2, $3,NOW())
	`

	_, err := r.db.Exec(ctx, query,
		id,
		req.Photo,
		req.Name,
	)

	if err != nil {
		return nil, err
	}

	return &car_service.Brand{
		Id:    id,
		Photo: req.Photo,
		Name:  req.Name,
	}, nil
}

func (r *BrandRepo) GetByID(ctx context.Context, req *car_service.BrandPrimaryKey) (*car_service.Brand, error) {

	// var whereField = "id"
	// if len(req.Username) > 0 {
	// 	whereField = "username"
	// 	req.Id = req.Username
	// }

	var (
		query string

		id         sql.NullString
		photo      sql.NullString
		name       sql.NullString
		created_at sql.NullString
		updated_at sql.NullString
	)

	query = `
		SELECT
			id,
			photo,
			name,
			created_at,
			updated_at		
		FROM brand
		WHERE id = $1
	`

	err := r.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&photo,
		&name,
		&created_at,
		&updated_at,
	)

	if err != nil {
		return nil, err
	}

	return &car_service.Brand{
		Id:        id.String,
		Photo:     photo.String,
		Name:      name.String,
		CreatedAt: created_at.String,
		UpdatedAt: updated_at.String,
	}, nil
}

func (r *BrandRepo) GetList(ctx context.Context, req *car_service.BrandGetListRequest) (*car_service.BrandGetListResponse, error) {

	var (
		resp   = &car_service.BrandGetListResponse{}
		query  string
		where  = " WHERE TRUE"
		offset = " OFFSET 0"
		limit  = " LIMIT 10"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			photo,
			name,
			created_at,
			updated_at		
		FROM brand
	`

	if req.Offset > 0 {
		offset = fmt.Sprintf(" OFFSET %d", req.Offset)
	}

	if req.Limit > 0 {
		limit = fmt.Sprintf(" LIMIT %d", req.Limit)
	}

	if req.Search != "" {
		where += ` AND name ILIKE '%' || '` + req.Search + `' || '%'`
	}

	query += where + offset + limit

	rows, err := r.db.Query(ctx, query)
	if err != nil {
		return nil, err
	}

	for rows.Next() {
		var (
			id         sql.NullString
			photo      sql.NullString
			name       sql.NullString
			created_at sql.NullString
			updated_at sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&photo,
			&name,
			&created_at,
			&updated_at,
		)

		if err != nil {
			return nil, err
		}

		resp.Brands = append(resp.Brands, &car_service.Brand{
			Id:        id.String,
			Photo:     photo.String,
			Name:      name.String,
			CreatedAt: created_at.String,
			UpdatedAt: updated_at.String,
		})
	}
	rows.Close()

	return resp, nil
}

func (r *BrandRepo) Update(ctx context.Context, req *car_service.BrandUpdate) (*car_service.Brand, error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
		UPDATE
			brand
		SET
			photo = :photo,
			name = :name,
			updated_at = NOW()
		WHERE id = :id
	`

	params = map[string]interface{}{
		"id":    req.Id,
		"photo": helper.NewNullString(req.Photo),
		"name":  helper.NewNullString(req.Name),
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := r.db.Exec(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	if result.RowsAffected() == 0 {
		return nil, nil
	}

	return &car_service.Brand{
		Id:    req.Id,
		Photo: req.Photo,
		Name:  req.Name,
	}, nil
}

func (r *BrandRepo) Delete(ctx context.Context, req *car_service.BrandPrimaryKey) error {

	_, err := r.db.Exec(ctx, "DELETE FROM brand WHERE id = $1", req.Id)
	if err != nil {
		return err
	}

	return nil
}
