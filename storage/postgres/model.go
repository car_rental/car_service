package postgres

import (
	"context"
	"database/sql"
	"fmt"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"

	"gitlab.com/car_rental/car_service/genproto/car_service"
	"gitlab.com/car_rental/car_service/pkg/helper"
)

type ModelRepo struct {
	db *pgxpool.Pool
}

func NewModelRepo(db *pgxpool.Pool) *ModelRepo {
	return &ModelRepo{
		db: db,
	}
}

func (r *ModelRepo) Create(ctx context.Context, req *car_service.ModelCreate) (*car_service.Model, error) {

	var (
		id    = uuid.New().String()
		query string
	)

	query = `
		INSERT INTO model(id, brand_id, name, updated_at)
		VALUES ($1, $2, $3,NOW())
	`

	_, err := r.db.Exec(ctx, query,
		id,
		req.BrandId,
		req.Name,
	)

	if err != nil {
		return nil, err
	}

	return &car_service.Model{
		Id:      id,
		BrandId: req.BrandId,
		Name:    req.Name,
	}, nil
}

func (r *ModelRepo) GetByID(ctx context.Context, req *car_service.ModelPrimaryKey) (*car_service.Model, error) {

	var (
		query string

		id         sql.NullString
		brandId    sql.NullString
		name       sql.NullString
		created_at sql.NullString
		updated_at sql.NullString
	)

	query = `
		SELECT
			id,
			brand_id,
			name,
			created_at,
			updated_at		
		FROM model
		WHERE id = $1
	`

	err := r.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&brandId,
		&name,
		&created_at,
		&updated_at,
	)

	if err != nil {
		return nil, err
	}

	return &car_service.Model{
		Id:        id.String,
		BrandId:   brandId.String,
		Name:      name.String,
		CreatedAt: created_at.String,
		UpdatedAt: updated_at.String,
	}, nil
}

func (r *ModelRepo) GetList(ctx context.Context, req *car_service.ModelGetListRequest) (*car_service.ModelGetListResponse, error) {

	var (
		resp   = &car_service.ModelGetListResponse{}
		query  string
		where  = " WHERE TRUE"
		offset = " OFFSET 0"
		limit  = " LIMIT 10"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			brand_id,
			name,
			created_at,
			updated_at		
		FROM model
	`

	if req.Offset > 0 {
		offset = fmt.Sprintf(" OFFSET %d", req.Offset)
	}

	if req.Limit > 0 {
		limit = fmt.Sprintf(" LIMIT %d", req.Limit)
	}

	if req.Search != "" {
		where += ` AND name ILIKE '%' || '` + req.Search + `' || '%'`
	}

	query += where + offset + limit

	rows, err := r.db.Query(ctx, query)
	if err != nil {
		return nil, err
	}

	for rows.Next() {
		var (
			id        sql.NullString
			brandId   sql.NullString
			name      sql.NullString
			createdAt sql.NullString
			updatedAt sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&brandId,
			&name,
			&createdAt,
			&updatedAt,
		)

		if err != nil {
			return nil, err
		}

		resp.Models = append(resp.Models, &car_service.Model{
			Id:        id.String,
			BrandId:   brandId.String,
			Name:      name.String,
			CreatedAt: createdAt.String,
			UpdatedAt: updatedAt.String,
		})
	}
	rows.Close()

	return resp, nil
}

func (r *ModelRepo) Update(ctx context.Context, req *car_service.ModelUpdate) (*car_service.Model, error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
		UPDATE
			model
		SET
			brand_id = :brandId,
			name = :name,
			updated_at = NOW()
		WHERE id = :id
	`

	params = map[string]interface{}{
		"id":      req.Id,
		"brandId": helper.NewNullString(req.BrandId),
		"name":    helper.NewNullString(req.Name),
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := r.db.Exec(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	if result.RowsAffected() == 0 {
		return nil, nil
	}

	return &car_service.Model{
		Id:      req.Id,
		BrandId: req.BrandId,
		Name:    req.Name,
	}, nil
}

func (r *ModelRepo) Delete(ctx context.Context, req *car_service.ModelPrimaryKey) error {

	_, err := r.db.Exec(ctx, "DELETE FROM model WHERE id = $1", req.Id)
	if err != nil {
		return err
	}

	return nil
}
