package postgres

import (
	"context"
	"database/sql"
	"fmt"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"

	"gitlab.com/car_rental/car_service/genproto/car_service"
	"gitlab.com/car_rental/car_service/pkg/helper"
)

type CarRepo struct {
	db *pgxpool.Pool
}

func NewCarRepo(db *pgxpool.Pool) *CarRepo {
	return &CarRepo{
		db: db,
	}
}

func (r *CarRepo) Create(ctx context.Context, req *car_service.CarCreate) (*car_service.Car, error) {

	var (
		id    = uuid.New().String()
		query string
	)

	query = `
		INSERT INTO car(id, brand_id, model_id, state_number, mileage, status ,investor_id, updated_at)
		VALUES ($1, $2, $3, $4, $5, $6, $7,NOW())
	`

	_, err := r.db.Exec(ctx, query,
		id,
		helper.NewNullString(req.BrandId),
		helper.NewNullString(req.ModelId),
		req.StateNumber,
		req.Mileage,
		req.Status,
		helper.NewNullString(req.InvestorId),
	)

	if err != nil {
		return nil, err
	}

	return &car_service.Car{
		Id:          id,
		BrandId:     req.BrandId,
		ModelId:     req.ModelId,
		StateNumber: req.StateNumber,
		Mileage:     req.Mileage,
		InvestorId:  req.InvestorId,
		Status:      req.Status,
	}, nil
}

func (r *CarRepo) GetByID(ctx context.Context, req *car_service.CarPrimaryKey) (*car_service.Car, error) {

	// var whereField = "id"
	// if len(req.Username) > 0 {
	// 	whereField = "username"
	// 	req.Id = req.Username
	// }

	var (
		query string

		id           sql.NullString
		brand_id     sql.NullString
		model_id     sql.NullString
		state_number sql.NullString
		mileage      sql.NullInt64
		investor_id  sql.NullString
		status       sql.NullString
		created_at   sql.NullString
		updated_at   sql.NullString
	)

	query = `
		SELECT
			id,
			brand_id,
			model_id,
			state_number,
			mileage,
			investor_id,
			status,
			created_at,
			updated_at		
		FROM car
		WHERE id = $1
	`

	err := r.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&brand_id,
		&model_id,
		&state_number,
		&mileage,
		&investor_id,
		&status,
		&created_at,
		&updated_at,
	)

	if err != nil {
		return nil, err
	}

	return &car_service.Car{
		Id:          id.String,
		BrandId:     brand_id.String,
		ModelId:     model_id.String,
		StateNumber: state_number.String,
		Mileage:     mileage.Int64,
		InvestorId:  investor_id.String,
		Status:      status.String,
		CreatedAt:   created_at.String,
		UpdatedAt:   updated_at.String,
	}, nil
}

func (r *CarRepo) GetList(ctx context.Context, req *car_service.CarGetListRequest) (*car_service.CarGetListResponse, error) {

	var (
		resp   = &car_service.CarGetListResponse{}
		query  string
		where  = " WHERE TRUE"
		offset = " OFFSET 0"
		limit  = " LIMIT 10"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			brand_id,
			model_id,
			state_number,
			mileage,
			investor_id,
			status,
			created_at,
			updated_at		
		FROM car
	`

	if req.Offset > 0 {
		offset = fmt.Sprintf(" OFFSET %d", req.Offset)
	}

	if req.Limit > 0 {
		limit = fmt.Sprintf(" LIMIT %d", req.Limit)
	}

	if req.SearchByBrand != "" {
		where += ` AND brand_id ILIKE '%' || '` + req.SearchByBrand + `' || '%'`
	}

	if req.SearchByInvestorId != "" {
		where += ` AND investor_id ILIKE '%' || '` + req.SearchByInvestorId + `' || '%'`
	}

	if req.SearchByModel != "" {
		where += ` AND model_id ILIKE '%' || '` + req.SearchByModel + `' || '%'`
	}

	if req.SearchByStateNumber != "" {
		where += ` AND state_number ILIKE '%' || '` + req.SearchByStateNumber + `' || '%'`
	}

	query += where + offset + limit

	rows, err := r.db.Query(ctx, query)
	if err != nil {
		return nil, err
	}

	for rows.Next() {
		var (
			id           sql.NullString
			brand_id     sql.NullString
			model_id     sql.NullString
			state_number sql.NullString
			mileage      sql.NullInt64
			investor_id  sql.NullString
			status       sql.NullString
			created_at   sql.NullString
			updated_at   sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&brand_id,
			&model_id,
			&state_number,
			&mileage,
			&investor_id,
			&status,
			&created_at,
			&updated_at,
		)

		if err != nil {
			return nil, err
		}

		resp.Cars = append(resp.Cars, &car_service.Car{
			Id:          id.String,
			BrandId:     brand_id.String,
			ModelId:     model_id.String,
			StateNumber: state_number.String,
			Mileage:     mileage.Int64,
			InvestorId:  investor_id.String,
			Status:      status.String,
			CreatedAt:   created_at.String,
			UpdatedAt:   updated_at.String,
		})
	}
	rows.Close()

	return resp, nil
}

func (r *CarRepo) Update(ctx context.Context, req *car_service.CarUpdate) (*car_service.Car, error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
		UPDATE
			car
		SET
			brand_id = :brand_id,
			model_id = :model_id,
			state_number = :state_number,
			mileage = :mileage,
			investor_id = :investor_id,		
			status = :status,
			updated_at = NOW()
		WHERE id = :id
	`

	params = map[string]interface{}{
		"id":           req.Id,
		"brand_id":     helper.NewNullString(req.BrandId),
		"model_id":     helper.NewNullString(req.ModelId),
		"state_number": req.StateNumber,
		"mileage":      req.Mileage,
		"investor_id":  helper.NewNullString(req.InvestorId),
		"status":       req.Status,
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := r.db.Exec(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	if result.RowsAffected() == 0 {
		return nil, nil
	}

	return &car_service.Car{
		Id:          req.Id,
		BrandId:     req.BrandId,
		ModelId:     req.ModelId,
		StateNumber: req.StateNumber,
		Mileage:     req.Mileage,
		InvestorId:  req.InvestorId,
		Status:      req.Status,
	}, nil
}

func (r *CarRepo) Delete(ctx context.Context, req *car_service.CarPrimaryKey) error {

	_, err := r.db.Exec(ctx, "DELETE FROM car WHERE id = $1", req.Id)
	if err != nil {
		return err
	}

	return nil
}
