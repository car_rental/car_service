CREATE TABLE car_report (
    state_number varchar,
    car_id  UUID REFERENCES car("id"),
    "status" varchar,
    "date" TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);