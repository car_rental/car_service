CREATE TABLE brand (
    id UUID PRIMARY KEY NOT NULL,
    photo varchar,
    "name" varchar(45),
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP
);
CREATE TABLE model (
    id UUID PRIMARY KEY NOT NULL,
    brand_id UUID REFERENCES brand("id"),
    "name" varchar(45),
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP
);
CREATE TABLE car (
    id UUID PRIMARY KEY NOT NULL,
    brand_id UUID REFERENCES brand("id"),
    model_id UUID REFERENCES model("id"),
    state_number varchar(45) NOT NULL,
    mileage int NOT NULL,
    investor_id UUID NOT NULL,
    "status" varchar DEFAULT 'in stock', 
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP
);