package service

import (
	"context"

	"github.com/golang/protobuf/ptypes/empty"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/emptypb"

	"gitlab.com/car_rental/car_service/config"
	"gitlab.com/car_rental/car_service/genproto/car_service"
	"gitlab.com/car_rental/car_service/grpc/client"
	"gitlab.com/car_rental/car_service/pkg/logger"
	"gitlab.com/car_rental/car_service/storage"
)

type CarService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*car_service.UnimplementedCarServiceServer
}

func NewCarService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) *CarService {
	return &CarService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvc,
	}
}

func (u *CarService) Create(ctx context.Context, req *car_service.CarCreate) (*car_service.Car, error) {
	u.log.Info("====== Car Create ======", logger.Any("req", req))

	resp, err := u.strg.Car().Create(ctx, req)
	if err != nil {
		u.log.Error("Error While Create Car: u.strg.Car().Create", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	// _, err = u.strg.CarActivity().Create(ctx, &car_service.CarActivityCreate{
	// 	StateNumber: req.StateNumber,
	// 	CarId:       resp.Id,
	// 	Status:      req.Status,
	// 	Date:        time.Now().Format("2006-01-02"),
	// })

	// if err != nil {
	// 	u.log.Error("Error While Create CarActivity: u.strg.Car().Create", logger.Error(err))
	// 	return nil, status.Error(codes.InvalidArgument, err.Error())
	// }

	return resp, nil
}

func (u *CarService) GetById(ctx context.Context, req *car_service.CarPrimaryKey) (*car_service.Car, error) {
	u.log.Info("====== Car Get By Id ======", logger.Any("req", req))

	resp, err := u.strg.Car().GetByID(ctx, req)
	if err != nil {
		u.log.Error("Error While Car Get By ID: u.strg.Car().GetByID", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *CarService) GetList(ctx context.Context, req *car_service.CarGetListRequest) (*car_service.CarGetListResponse, error) {
	u.log.Info("====== Car Get List ======", logger.Any("req", req))

	resp, err := u.strg.Car().GetList(ctx, req)
	if err != nil {
		u.log.Error("Error While Car Get List: u.strg.Car().GetList", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *CarService) Update(ctx context.Context, req *car_service.CarUpdate) (*car_service.Car, error) {
	u.log.Info("====== Car Update ======", logger.Any("req", req))

	resp, err := u.strg.Car().Update(ctx, req)
	if err != nil {
		u.log.Error("Error While Car Update: u.strg.Car().Update", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *CarService) Delete(ctx context.Context, req *car_service.CarPrimaryKey) (*emptypb.Empty, error) {
	u.log.Info("====== Car Delete ======", logger.Any("req", req))

	err := u.strg.Car().Delete(ctx, req)
	if err != nil {
		u.log.Error("Error While Car Delete: u.strg.Car().Delete", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return &empty.Empty{}, nil
}
