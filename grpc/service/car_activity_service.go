package service

import (
	"context"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	"gitlab.com/car_rental/car_service/config"
	"gitlab.com/car_rental/car_service/genproto/car_service"
	"gitlab.com/car_rental/car_service/grpc/client"
	"gitlab.com/car_rental/car_service/pkg/logger"
	"gitlab.com/car_rental/car_service/storage"
)

type CarActivityService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*car_service.UnimplementedCarActivityServiceServer
}

func NewCarActivityService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) *CarActivityService {
	return &CarActivityService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvc,
	}
}

func (u *CarActivityService) Create(ctx context.Context, req *car_service.CarActivityCreate) (*car_service.CarActivity, error) {
	u.log.Info("====== CarActivity Create ======", logger.Any("req", req))

	resp, err := u.strg.CarActivity().Create(ctx, req)
	if err != nil {
		u.log.Error("Error While Create CarActivity: u.strg.CarActivity().Create", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *CarActivityService) GetList(ctx context.Context, req *car_service.CarActivityGetListRequest) (*car_service.CarsActivityResponse, error) {
	u.log.Info("====== CarActivity Get List ======", logger.Any("req", req))

	resp, err := u.strg.CarActivity().GetList(ctx, req)
	if err != nil {
		u.log.Error("Error While CarActivity Get List: u.strg.CarActivity().GetList", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	carReports := []*car_service.CarActivityResp{}

	hash := map[string][]*car_service.CarActivity{}

	for _, data := range resp.Res {
		_, ok := hash[data.StateNumber]
		if ok {
			hash[data.StateNumber] = append(hash[data.StateNumber], data)
		} else {
			hash[data.StateNumber] = []*car_service.CarActivity{data}
		}
	}

	for key, value := range hash {
		carReports = append(carReports, &car_service.CarActivityResp{
			StateNumber: key,
			Res:         value,
		})
	}

	response := &car_service.CarsActivityResponse{
		CarReports: carReports,
	}

	return response, nil
}
