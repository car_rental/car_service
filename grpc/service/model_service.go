package service

import (
	"context"

	"github.com/golang/protobuf/ptypes/empty"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/emptypb"

	"gitlab.com/car_rental/car_service/config"
	"gitlab.com/car_rental/car_service/genproto/car_service"
	"gitlab.com/car_rental/car_service/grpc/client"
	"gitlab.com/car_rental/car_service/pkg/logger"
	"gitlab.com/car_rental/car_service/storage"
)

type ModelService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*car_service.UnimplementedModelServiceServer
}

func NewModelService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) *ModelService {
	return &ModelService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvc,
	}
}

func (u *ModelService) Create(ctx context.Context, req *car_service.ModelCreate) (*car_service.Model, error) {
	u.log.Info("====== Model Create ======", logger.Any("req", req))

	resp, err := u.strg.Model().Create(ctx, req)
	if err != nil {
		u.log.Error("Error While Create Model: u.strg.Model().Create", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *ModelService) GetById(ctx context.Context, req *car_service.ModelPrimaryKey) (*car_service.Model, error) {
	u.log.Info("====== Model Get By Id ======", logger.Any("req", req))

	resp, err := u.strg.Model().GetByID(ctx, req)
	if err != nil {
		u.log.Error("Error While Model Get By ID: u.strg.Model().GetByID", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *ModelService) GetList(ctx context.Context, req *car_service.ModelGetListRequest) (*car_service.ModelGetListResponse, error) {
	u.log.Info("====== Model Get List ======", logger.Any("req", req))

	resp, err := u.strg.Model().GetList(ctx, req)
	if err != nil {
		u.log.Error("Error While Model Get List: u.strg.Model().GetList", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *ModelService) Update(ctx context.Context, req *car_service.ModelUpdate) (*car_service.Model, error) {
	u.log.Info("====== Model Update ======", logger.Any("req", req))

	resp, err := u.strg.Model().Update(ctx, req)
	if err != nil {
		u.log.Error("Error While Model Update: u.strg.Model().Update", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *ModelService) Delete(ctx context.Context, req *car_service.ModelPrimaryKey) (*emptypb.Empty, error) {
	u.log.Info("====== Model Delete ======", logger.Any("req", req))

	err := u.strg.Model().Delete(ctx, req)
	if err != nil {
		u.log.Error("Error While Model Delete: u.strg.Model().Delete", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return &empty.Empty{}, nil
}
