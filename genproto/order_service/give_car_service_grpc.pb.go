// Code generated by protoc-gen-go-grpc. DO NOT EDIT.
// versions:
// - protoc-gen-go-grpc v1.2.0
// - protoc             v3.21.12
// source: give_car_service.proto

package order_service

import (
	context "context"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
	emptypb "google.golang.org/protobuf/types/known/emptypb"
)

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
// Requires gRPC-Go v1.32.0 or later.
const _ = grpc.SupportPackageIsVersion7

// GiveCarServiceClient is the client API for GiveCarService service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://pkg.go.dev/google.golang.org/grpc/?tab=doc#ClientConn.NewStream.
type GiveCarServiceClient interface {
	Create(ctx context.Context, in *GiveCarCreate, opts ...grpc.CallOption) (*GiveCar, error)
	GetById(ctx context.Context, in *GiveCarPrimaryKey, opts ...grpc.CallOption) (*GiveCar, error)
	GetList(ctx context.Context, in *GiveCarGetListRequest, opts ...grpc.CallOption) (*GiveCarGetListResponse, error)
	Update(ctx context.Context, in *GiveCarUpdate, opts ...grpc.CallOption) (*GiveCar, error)
	Delete(ctx context.Context, in *GiveCarPrimaryKey, opts ...grpc.CallOption) (*emptypb.Empty, error)
}

type giveCarServiceClient struct {
	cc grpc.ClientConnInterface
}

func NewGiveCarServiceClient(cc grpc.ClientConnInterface) GiveCarServiceClient {
	return &giveCarServiceClient{cc}
}

func (c *giveCarServiceClient) Create(ctx context.Context, in *GiveCarCreate, opts ...grpc.CallOption) (*GiveCar, error) {
	out := new(GiveCar)
	err := c.cc.Invoke(ctx, "/order_service.GiveCarService/Create", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *giveCarServiceClient) GetById(ctx context.Context, in *GiveCarPrimaryKey, opts ...grpc.CallOption) (*GiveCar, error) {
	out := new(GiveCar)
	err := c.cc.Invoke(ctx, "/order_service.GiveCarService/GetById", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *giveCarServiceClient) GetList(ctx context.Context, in *GiveCarGetListRequest, opts ...grpc.CallOption) (*GiveCarGetListResponse, error) {
	out := new(GiveCarGetListResponse)
	err := c.cc.Invoke(ctx, "/order_service.GiveCarService/GetList", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *giveCarServiceClient) Update(ctx context.Context, in *GiveCarUpdate, opts ...grpc.CallOption) (*GiveCar, error) {
	out := new(GiveCar)
	err := c.cc.Invoke(ctx, "/order_service.GiveCarService/Update", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *giveCarServiceClient) Delete(ctx context.Context, in *GiveCarPrimaryKey, opts ...grpc.CallOption) (*emptypb.Empty, error) {
	out := new(emptypb.Empty)
	err := c.cc.Invoke(ctx, "/order_service.GiveCarService/Delete", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// GiveCarServiceServer is the server API for GiveCarService service.
// All implementations must embed UnimplementedGiveCarServiceServer
// for forward compatibility
type GiveCarServiceServer interface {
	Create(context.Context, *GiveCarCreate) (*GiveCar, error)
	GetById(context.Context, *GiveCarPrimaryKey) (*GiveCar, error)
	GetList(context.Context, *GiveCarGetListRequest) (*GiveCarGetListResponse, error)
	Update(context.Context, *GiveCarUpdate) (*GiveCar, error)
	Delete(context.Context, *GiveCarPrimaryKey) (*emptypb.Empty, error)
	mustEmbedUnimplementedGiveCarServiceServer()
}

// UnimplementedGiveCarServiceServer must be embedded to have forward compatible implementations.
type UnimplementedGiveCarServiceServer struct {
}

func (UnimplementedGiveCarServiceServer) Create(context.Context, *GiveCarCreate) (*GiveCar, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Create not implemented")
}
func (UnimplementedGiveCarServiceServer) GetById(context.Context, *GiveCarPrimaryKey) (*GiveCar, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetById not implemented")
}
func (UnimplementedGiveCarServiceServer) GetList(context.Context, *GiveCarGetListRequest) (*GiveCarGetListResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetList not implemented")
}
func (UnimplementedGiveCarServiceServer) Update(context.Context, *GiveCarUpdate) (*GiveCar, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Update not implemented")
}
func (UnimplementedGiveCarServiceServer) Delete(context.Context, *GiveCarPrimaryKey) (*emptypb.Empty, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Delete not implemented")
}
func (UnimplementedGiveCarServiceServer) mustEmbedUnimplementedGiveCarServiceServer() {}

// UnsafeGiveCarServiceServer may be embedded to opt out of forward compatibility for this service.
// Use of this interface is not recommended, as added methods to GiveCarServiceServer will
// result in compilation errors.
type UnsafeGiveCarServiceServer interface {
	mustEmbedUnimplementedGiveCarServiceServer()
}

func RegisterGiveCarServiceServer(s grpc.ServiceRegistrar, srv GiveCarServiceServer) {
	s.RegisterService(&GiveCarService_ServiceDesc, srv)
}

func _GiveCarService_Create_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(GiveCarCreate)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(GiveCarServiceServer).Create(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/order_service.GiveCarService/Create",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(GiveCarServiceServer).Create(ctx, req.(*GiveCarCreate))
	}
	return interceptor(ctx, in, info, handler)
}

func _GiveCarService_GetById_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(GiveCarPrimaryKey)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(GiveCarServiceServer).GetById(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/order_service.GiveCarService/GetById",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(GiveCarServiceServer).GetById(ctx, req.(*GiveCarPrimaryKey))
	}
	return interceptor(ctx, in, info, handler)
}

func _GiveCarService_GetList_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(GiveCarGetListRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(GiveCarServiceServer).GetList(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/order_service.GiveCarService/GetList",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(GiveCarServiceServer).GetList(ctx, req.(*GiveCarGetListRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _GiveCarService_Update_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(GiveCarUpdate)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(GiveCarServiceServer).Update(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/order_service.GiveCarService/Update",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(GiveCarServiceServer).Update(ctx, req.(*GiveCarUpdate))
	}
	return interceptor(ctx, in, info, handler)
}

func _GiveCarService_Delete_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(GiveCarPrimaryKey)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(GiveCarServiceServer).Delete(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/order_service.GiveCarService/Delete",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(GiveCarServiceServer).Delete(ctx, req.(*GiveCarPrimaryKey))
	}
	return interceptor(ctx, in, info, handler)
}

// GiveCarService_ServiceDesc is the grpc.ServiceDesc for GiveCarService service.
// It's only intended for direct use with grpc.RegisterService,
// and not to be introspected or modified (even as a copy)
var GiveCarService_ServiceDesc = grpc.ServiceDesc{
	ServiceName: "order_service.GiveCarService",
	HandlerType: (*GiveCarServiceServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "Create",
			Handler:    _GiveCarService_Create_Handler,
		},
		{
			MethodName: "GetById",
			Handler:    _GiveCarService_GetById_Handler,
		},
		{
			MethodName: "GetList",
			Handler:    _GiveCarService_GetList_Handler,
		},
		{
			MethodName: "Update",
			Handler:    _GiveCarService_Update_Handler,
		},
		{
			MethodName: "Delete",
			Handler:    _GiveCarService_Delete_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "give_car_service.proto",
}
