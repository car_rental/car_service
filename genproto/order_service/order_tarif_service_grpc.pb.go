// Code generated by protoc-gen-go-grpc. DO NOT EDIT.
// versions:
// - protoc-gen-go-grpc v1.2.0
// - protoc             v3.21.12
// source: order_tarif_service.proto

package order_service

import (
	context "context"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
	emptypb "google.golang.org/protobuf/types/known/emptypb"
)

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
// Requires gRPC-Go v1.32.0 or later.
const _ = grpc.SupportPackageIsVersion7

// OrderTarifServiceClient is the client API for OrderTarifService service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://pkg.go.dev/google.golang.org/grpc/?tab=doc#ClientConn.NewStream.
type OrderTarifServiceClient interface {
	Create(ctx context.Context, in *OrderTarifCreate, opts ...grpc.CallOption) (*OrderTarif, error)
	GetById(ctx context.Context, in *OrderTarifPrimaryKey, opts ...grpc.CallOption) (*OrderTarif, error)
	GetList(ctx context.Context, in *OrderTarifGetListRequest, opts ...grpc.CallOption) (*OrderTarifGetListResponse, error)
	Update(ctx context.Context, in *OrderTarifUpdate, opts ...grpc.CallOption) (*OrderTarif, error)
	Delete(ctx context.Context, in *OrderTarifPrimaryKey, opts ...grpc.CallOption) (*emptypb.Empty, error)
}

type orderTarifServiceClient struct {
	cc grpc.ClientConnInterface
}

func NewOrderTarifServiceClient(cc grpc.ClientConnInterface) OrderTarifServiceClient {
	return &orderTarifServiceClient{cc}
}

func (c *orderTarifServiceClient) Create(ctx context.Context, in *OrderTarifCreate, opts ...grpc.CallOption) (*OrderTarif, error) {
	out := new(OrderTarif)
	err := c.cc.Invoke(ctx, "/order_service.OrderTarifService/Create", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *orderTarifServiceClient) GetById(ctx context.Context, in *OrderTarifPrimaryKey, opts ...grpc.CallOption) (*OrderTarif, error) {
	out := new(OrderTarif)
	err := c.cc.Invoke(ctx, "/order_service.OrderTarifService/GetById", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *orderTarifServiceClient) GetList(ctx context.Context, in *OrderTarifGetListRequest, opts ...grpc.CallOption) (*OrderTarifGetListResponse, error) {
	out := new(OrderTarifGetListResponse)
	err := c.cc.Invoke(ctx, "/order_service.OrderTarifService/GetList", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *orderTarifServiceClient) Update(ctx context.Context, in *OrderTarifUpdate, opts ...grpc.CallOption) (*OrderTarif, error) {
	out := new(OrderTarif)
	err := c.cc.Invoke(ctx, "/order_service.OrderTarifService/Update", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *orderTarifServiceClient) Delete(ctx context.Context, in *OrderTarifPrimaryKey, opts ...grpc.CallOption) (*emptypb.Empty, error) {
	out := new(emptypb.Empty)
	err := c.cc.Invoke(ctx, "/order_service.OrderTarifService/Delete", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// OrderTarifServiceServer is the server API for OrderTarifService service.
// All implementations must embed UnimplementedOrderTarifServiceServer
// for forward compatibility
type OrderTarifServiceServer interface {
	Create(context.Context, *OrderTarifCreate) (*OrderTarif, error)
	GetById(context.Context, *OrderTarifPrimaryKey) (*OrderTarif, error)
	GetList(context.Context, *OrderTarifGetListRequest) (*OrderTarifGetListResponse, error)
	Update(context.Context, *OrderTarifUpdate) (*OrderTarif, error)
	Delete(context.Context, *OrderTarifPrimaryKey) (*emptypb.Empty, error)
	mustEmbedUnimplementedOrderTarifServiceServer()
}

// UnimplementedOrderTarifServiceServer must be embedded to have forward compatible implementations.
type UnimplementedOrderTarifServiceServer struct {
}

func (UnimplementedOrderTarifServiceServer) Create(context.Context, *OrderTarifCreate) (*OrderTarif, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Create not implemented")
}
func (UnimplementedOrderTarifServiceServer) GetById(context.Context, *OrderTarifPrimaryKey) (*OrderTarif, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetById not implemented")
}
func (UnimplementedOrderTarifServiceServer) GetList(context.Context, *OrderTarifGetListRequest) (*OrderTarifGetListResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetList not implemented")
}
func (UnimplementedOrderTarifServiceServer) Update(context.Context, *OrderTarifUpdate) (*OrderTarif, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Update not implemented")
}
func (UnimplementedOrderTarifServiceServer) Delete(context.Context, *OrderTarifPrimaryKey) (*emptypb.Empty, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Delete not implemented")
}
func (UnimplementedOrderTarifServiceServer) mustEmbedUnimplementedOrderTarifServiceServer() {}

// UnsafeOrderTarifServiceServer may be embedded to opt out of forward compatibility for this service.
// Use of this interface is not recommended, as added methods to OrderTarifServiceServer will
// result in compilation errors.
type UnsafeOrderTarifServiceServer interface {
	mustEmbedUnimplementedOrderTarifServiceServer()
}

func RegisterOrderTarifServiceServer(s grpc.ServiceRegistrar, srv OrderTarifServiceServer) {
	s.RegisterService(&OrderTarifService_ServiceDesc, srv)
}

func _OrderTarifService_Create_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(OrderTarifCreate)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(OrderTarifServiceServer).Create(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/order_service.OrderTarifService/Create",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(OrderTarifServiceServer).Create(ctx, req.(*OrderTarifCreate))
	}
	return interceptor(ctx, in, info, handler)
}

func _OrderTarifService_GetById_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(OrderTarifPrimaryKey)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(OrderTarifServiceServer).GetById(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/order_service.OrderTarifService/GetById",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(OrderTarifServiceServer).GetById(ctx, req.(*OrderTarifPrimaryKey))
	}
	return interceptor(ctx, in, info, handler)
}

func _OrderTarifService_GetList_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(OrderTarifGetListRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(OrderTarifServiceServer).GetList(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/order_service.OrderTarifService/GetList",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(OrderTarifServiceServer).GetList(ctx, req.(*OrderTarifGetListRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _OrderTarifService_Update_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(OrderTarifUpdate)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(OrderTarifServiceServer).Update(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/order_service.OrderTarifService/Update",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(OrderTarifServiceServer).Update(ctx, req.(*OrderTarifUpdate))
	}
	return interceptor(ctx, in, info, handler)
}

func _OrderTarifService_Delete_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(OrderTarifPrimaryKey)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(OrderTarifServiceServer).Delete(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/order_service.OrderTarifService/Delete",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(OrderTarifServiceServer).Delete(ctx, req.(*OrderTarifPrimaryKey))
	}
	return interceptor(ctx, in, info, handler)
}

// OrderTarifService_ServiceDesc is the grpc.ServiceDesc for OrderTarifService service.
// It's only intended for direct use with grpc.RegisterService,
// and not to be introspected or modified (even as a copy)
var OrderTarifService_ServiceDesc = grpc.ServiceDesc{
	ServiceName: "order_service.OrderTarifService",
	HandlerType: (*OrderTarifServiceServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "Create",
			Handler:    _OrderTarifService_Create_Handler,
		},
		{
			MethodName: "GetById",
			Handler:    _OrderTarifService_GetById_Handler,
		},
		{
			MethodName: "GetList",
			Handler:    _OrderTarifService_GetList_Handler,
		},
		{
			MethodName: "Update",
			Handler:    _OrderTarifService_Update_Handler,
		},
		{
			MethodName: "Delete",
			Handler:    _OrderTarifService_Delete_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "order_tarif_service.proto",
}
